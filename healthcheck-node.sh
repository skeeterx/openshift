#!/bin/bash

# Pedro Verissimo Dantas Neto (pedro@verissimo.net.br)
# Date: 25/08/2020
#
# Descrição: Script criado para checar a saúde de cada node
# Testes realizados na versão do Openshift 3.11
# Version       : 1.0
#

# Checagem do status de cada nodo em cada type disponivel
oc get node -o=jsonpath='{range .items[*]}{..conditions[?(@.type=="DiskPressure")].status}{..conditions[?(@.type=="OutOfDisk")].status}{..conditions[?(@.type=="MemoryPressure")].status}{..conditions[?(@.type=="PIDPressure")].status}{"\n"}{end}' | grep True > /dev/null 2>&1
validacao=$?

if [ $validacao -eq 0 ]; then

  listar_nodos=`oc get node -o jsonpath='{range .items[*]}{.metadata.name}{"\n"}{end}'`

  for nodo in $listar_nodos; do
    result_DiskPressure=`oc get node $nodo -o=jsonpath='{..conditions[?(@.type=="DiskPressure")].status}'`
    if [ $result_DiskPressure == "True" ]; then
      echo "O $nodo possui um alerta do tipo DiskPressure:"
      echo `oc get node $nodo -o=jsonpath='{..conditions[?(@.type=="DiskPressure")].message}'`
    fi
    result_OutOfDisk=`oc get node $nodo -o=jsonpath='{..conditions[?(@.type=="OutOfDisk")].status}'`
    if [ $result_OutOfDisk == "True" ]; then
      echo "O $nodo possui um alerta do tipo OutOfDisk:"
      echo `oc get node $nodo -o=jsonpath='{..conditions[?(@.type=="OutOfDisk")].message}'`
    fi
    result_MemoryPressure=`oc get node $nodo -o=jsonpath='{..conditions[?(@.type=="MemoryPressure")].status}'`
    if [ $result_MemoryPressure == "True" ]; then
      echo "O $nodo possui um alerta do tipo MemoryPressure:"
      echo `oc get node $nodo -o=jsonpath='{..conditions[?(@.type=="MemoryPressure")].message}'`
    fi
    result_PIDPressure=`oc get node $nodo -o=jsonpath='{..conditions[?(@.type=="PIDPressure")].status}'`
    if [ $result_PIDPressure == "True" ]; then
      echo "O $nodo possui um alerta do tipo PIDPressure:"
      echo `oc get node $nodo -o=jsonpath='{..conditions[?(@.type=="PIDPressure")].message}'`
    fi
    echo ""
  done

  exit 1

else

  echo "A saúde de todos os nodos estão Ok!"
  exit 0

fi